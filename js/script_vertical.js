$('.item_menu:eq(0)').css({'opacity':'1'});

var_height = 600;
function scrollBox (section) {
	if(section==1) {
			scroll_vertical(0);
			$('.item_menu').css({'opacity':'0.6'});
			$('.item_menu:eq(0)').css({'opacity':'1'});
			$('video')[0].pause();
			$('video')[0].currentTime = 0;
	}
	if(section==2) {
			scroll_vertical(var_height);
			$('.item_menu').css({'opacity':'0.6'});
			$('.item_menu:eq(1)').css({'opacity':'1'});
	}
	if(section==3) {
			scroll_vertical(var_height*2);
			$('.item_menu').css({'opacity':'0.6'});
			$('.item_menu:eq(2)').css({'opacity':'1'});
			$('video')[0].pause();
			$('video')[0].currentTime = 0;
	}
}

/**
 * function scroll animation
 * @params var_goto integer scroll sampai mana
 * @return null
 */
animation_scroll_time = 1000;
function scroll_vertical(var_goto)
{
	if($('.show-off').scrollTop() != var_goto)
	{
	    $('.show-off').animate({
	        scrollTop: var_goto
	    }, animation_scroll_time);
	}
}


function playPause()
{
	if($('video')[0].paused)
		$('video')[0].play()
	else
		$('video')[0].pause();
};